#!/bin/bash
#"                                                             "#
##""                                                         ""##
###"""                                                     """###
#################################################################
#           CLA Technologies Git Manager for GitLab             #
#                       Made in House                           #
#                 Let's Create ;) - Open together               #
#                         It's FOSS                             #
#################################################################
###"""                                                     """###
##""                                                         ""##
#"                                                             "#

DATE=$(date)
CURRENTDIR=$(pwd)
HLOCATE=~
FILE=~/.gitconfig
GITCREDENTIALS=~/.git-credentials
LOGS=~/Desktop/logusgeneralis.log
FILETABLE=$CURRENTDIR/file_table.txt


if test -f "$FILE"; then
    echo ".gitconfig file found"
else
    echo "not found"
    kdialog --icon "preferences-git" --title "Git account - CLA Technologies" --msgbox ".gitconfig not found in $HLOCATE. Please configure gitlab account first."
fi

if test -f "$LOGS"; then
    echo "found"
else
    echo "
    #""***********************************************************""#
    ##""""*****************************************************""""##
    ###""""""***********************************************""""""###
    #################################################################
    #         CLA Technologies Git Manager for GitLab - LOGS        #
    #                       Made in House                           #
    #                 Let's Create ;) - Open together               #
    #                         It's FOSS                             #
    #################################################################
    ###""""""***********************************************""""""###
    ##""""*****************************************************""""##
    #""***********************************************************""#
    " >> $LOGS
    echo "not found, intro written"
fi

while [ $? != "1" ]
do

MENU=$(kdialog --icon "preferences-git" --title "GitLab Manager" --menu "What would you like to do?" -- 1 "Update GitLab folder" 2 "Upload to GitLab repo" 3 "Change GitLab Login info" 4 "Remove GitLab login save" 5 "Quit menu" 6 "View logs" 7 "Write a filetable tree")
echo $MENU
echo "$?"

case $MENU in
1)
git pull -v > verbose.txt 2>&1
echo "
Git updated at $DATE.
" >> $LOGS
cat verbose.txt >> $LOGS
echo "updated"
kdialog --icon "preferences-git" --title "GitLab verbose output" --textbox verbose.txt 512 256
;;

2)
git add .
GITCOMMIT=$(kdialog --icon "preferences-git" --title "GitLab push update" --inputbox "Enter any commit comments or click ok for default with date: " "commit")
if [ $GITCOMMIT = "commit" ]; then
  echo "Default chosen @ $DATE"
  git commit -m "Commited at $DATE"
else
  echo "$GITCOMMIT"
  git commit -m "$GITCOMMIT"
fi
git push -v > verbose.txt 2>&1
echo "
Git uploaded/modified at $DATE.
" >> $LOGS
cat verbose.txt >> $LOGS
kdialog --icon "preferences-git" --title "Gitlab verbose output" --textbox verbose.txt 512 256
;;

3)
kdialog --icon "preferences-git" --title "Git manager for GitLab - CLA Technologies" --warningcontinuecancel "Warning, the file responsible for saving login info is publically visible (.txt). Proceed with caution and security.
We are not responsible for any stolen info."
chose=$(echo $?)
if [ $chose = 0 ]; then
  USERNAME=$(kdialog --icon "preferences-git" --title "GitLab Account" --inputbox "Enter account username: " "username")
  EMAIL=$(kdialog --icon "preferences-git" --title "GitLab Account" --inputbox "Enter git e-mail address: " "e-mail")
  PASSWORD=$(kdialog --icon "preferences-git" --title "GitLab Account" --password "Please enter the server access code:")
  echo "https://$USERNAME:$PASSWORD@gitlab.com" > $GITCREDENTIALS
  echo "[user]
	email = $EMAIL
	name = $USERNAME
[credential]
	helper = store" > $FILE
  echo "
Git login for user ($USERNAME) modified at $DATE.
  " >> $LOGS
  echo "Login modified/created"
  kdialog --icon "preferences-git" --title "GitLab Account - CLA Technologies" --msgbox "Account information created to home directory."
else
    echo "canceled"
fi
;;

4)
if test -f "$GITCREDENTIALS"; then
    echo "found"
    rm ~/.git-credentials
    rm ~/.gitconfig
    echo "
Login details removed at $DATE
    " >> $LOGS
    kdialog --icon "preferences-git" --title "GitLab Account - CLA Technologies" --msgbox "Account information removed from home directory."
else
echo "
Attempted removal at $DATE. $GITCREDENTIALS not found.
" >> $LOGS
kdialog --icon "preferences-git" --title "GitLab Account - CLA Technologies" --error "Attempted removal at $DATE. $GITCREDENTIALS not found."
fi
;;

5) echo "quit"
echo "
Application quit at $DATE
" >> $LOGS
exit 0
;;


6)
kdialog --icon "preferences-git" --title "Git manager - Log viewer" --textbox $LOGS 720 480
;;

7)
kdialog --icon "preferences-git" --title "GitLab Account - CLA Technologies" --yesno "File table written. Open file_table.txt?                  "
FTAB=$(echo $?)
echo "---- FILE TABLE TREE - LISTED CONTENTS ----
" > file_table.txt
tree >> file_table.txt
echo "File table written."
  echo "
Filetable updated at $DATE
  " >> $LOGS
if [ $FTAB = 0 ]; then
  kdialog --icon "preferences-git" --title "Git manager - Log viewer" --textbox file_table.txt 720 480
else
  echo "not oppened"
fi
;;

*)
kdialog --icon "preferences-git" --title "Git manager" --passivepopup \
"The application has crashed. UNKNOWN" 10
echo "
Application crashed at $DATE
" >> $LOGS
echo "Something went wrong X_X" && exit 0
;;
esac
done
