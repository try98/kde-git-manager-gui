# KDE Git manager GUI

[![Platform: GNU/Linux](https://img.shields.io/badge/platform-GNU/Linux-blue.svg)](https://www.kernel.org/linux.html) [![License: GPL v2](https://img.shields.io/badge/License-GPLv2-green.svg)](https://www.gnu.org/licenses/gpl-2.0)

GUI Git configurator for GitLab using Kdialog. 
Menu style programm with options like: create/modify account, update git folder, upload changes, logs, verbose output and more.

Re-initialize git in the appropriated folder and place the script in the root of the git folder.
For maximum compatibility clone and then initialize git in the terminal

Before executing make sure that in the file properties the "is executable" option is ticked

CLI metheod:
```bash
chmod +x <filename>
```

# For non-KDE Desktop enviroments

Debian based systems:
```bash
sudo apt install kdialog
```

## -- IGNORE --

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) Checklist
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) Checklist


# Image

![open image](/welcome.png)
